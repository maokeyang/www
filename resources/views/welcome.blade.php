@extends('layouts.app')

@section('content')
    <div class="header">
        <div class="nav">
            <ul>
                <li><a href="/"><img class="site_logo" src="http://img.6chen.cn/1.png"></a></li>
                <li><a href="/works">Work</a></li>
                <li><a href="http://blog.6chen.cn" target=_blank>Blog</a></li>
            </ul>
        </div>
        <div class="info">
            <h1 class="site_title">莲生</h1>
            <p class="site_description">Visual Designer 🐷</p>
            <a class="telegram" href="http://blog.6chen.cn">Change yourself</a> <div class="social">


                <a class="level-item" href="http://weibo.com/webkim" target=_blank>
            <span class="icon">
                <i class="fa fa-weibo"></i>
            </span>
                </a>

                <a class="level-item" href="https://dribbble.com/kanso" target=_blank>
            <span class="icon">
                <i class="fa fa-dribbble"></i>
            </span>
                </a>

                <a class="level-item" href="https://www.instagram.com/w55ord/" target=_blank>
            <span class="icon">
                <i class="fa fa-instagram"></i>
            </span>
                </a>

            </div>
        </div>
    </div>
    <div class="work">
        <h2>About</h2>
        <p>放荡不羁的设计爱好者，雨露均沾的跨界杂家
            <br>用文字记录生活，用手作温暖生活，用料理丰盈生活
            <br>A面是淡然安静懒于交际的宅男，B面是用旅行充电用摄影表达的浪人
        </p>
        <div class="p_links">
            <li>

            </li>



        </div>
    </div>

    <div class="work_photo">
        <img class="w_p is-3" src="http://img.6chen.cn/image/pic_05.jpg" data-action="zoom" style="cursor: -webkit-zoom-in;">
        <img class="w_p is-3" src="http://img.6chen.cn/image/pic_06.jpg" data-action="zoom" style="cursor: -webkit-zoom-in;">
        <img class="w_p is-3" src="http://img.6chen.cn/image/pic_08.jpg" data-action="zoom" style="cursor: -webkit-zoom-in;">
        <img class="w_p is-3" src="http://img.6chen.cn/image/pic_04.jpg" data-action="zoom" style="cursor: -webkit-zoom-in;">
    </div>

    <div class="read_more">

          <br>在装逼和逗比之间无缝切换，在大众与小众之间来去自如
          <br>追求审美，简单生活
            <br></p>

    </div>

    <div class="experience">
        <h2>Experience</h2>
        <div class="xxx">
            <img class="w_p_100" src="http://img.6chen.cn/pic_05.jpg" data-action="zoom" style="cursor: -webkit-zoom-in;">
            <p>The position is constantly changing, but more and more interested in design.
                <br>If you want to talk to me, please <a href="mailto:542186947@qq.com">contact me</a>
                </p>
        </div>
        <div class="year">
            <div>
                <h3>2014-2019</h3>
                <div class="company">
                    <p><a href="http://www.youxiake.com">Youxiake</a> - UI Designer</p>
                    <h3>2011-2014</h3>
                    <div class="company">
                        <p><a href="#">格文数字</a> - Web Designer</p>
                </div>
            </div>

            </div>

                <div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="container has-text-centered">
                    <p>Designed and Coded by © CaiCai
                    <br><a href="http://www.beian.miit.gov.cn/" target=_blank>浙ICP备19043988号</a></p>
                </div>
            </div>
        </div>
    </div>
@endsection
