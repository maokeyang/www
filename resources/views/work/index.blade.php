@extends('layouts.app')

@section('content')
    <div class="main">
        <div class="nav">
            <ul>
                <li><a href="/"><img class="site_logo" src="http://img.6chen.cn/1.png"></a></li>
                <li><a href="/works">Work</a></li>
                <li><a href="http://blog.6chen.cn" target=_blank>Blog</a></li>
                <li><a href="https://dribbble.com/kanso" target=_blank>Dribbble</a></li>
            </ul>
        </div>
    </div>
    <div class="works">
        <div class="column">
            <h1>Works</h1>
            <p>设计的芬芳之于眼睛正如糖果的味道之于嘴.<span class="discourse_author">  </span></p>
        </div>
        <div>

          <div class="columns">
              <div class="list">
                  <img class="p_img" src="http://img.6chen.cn/image/work_07.jpg" data-action="zoom" style="cursor: -webkit-zoom-in;">
              </div>
          </div>

          <div class="title">
              <h4 class="subtitle">June 15, 2019</h4>
              <h3 class="p_title"><p>Birdboy emoji</p></h3>


              <p class="description">把身边的朋友用表情包的方式记录下来.</p>
              <a class="links" target="view_window" href="https://sticker.weixin.qq.com/cgi-bin/mmemoticon-bin/emoticonview?oper=single&t=recommend&productid=aL2PCfwK/89qO7sF6/+I+UDhfwEjhec2ZNvdnLLJRd/MCcycb2d75q+WyA2ASTtyuXlVgHwm1EkLcEALBsEHvfubWjG1EKRwWYm8fASL6ueA=">Visit website &gt;</a>
          </div>
        </div>
        </div>



            <div class="columns">
                <div class="list">
                    <img class="p_img" src="http://img.6chen.cn/work_01.png" data-action="zoom" style="cursor: -webkit-zoom-in;">
                </div>
            </div>

            <div class="title">
                <h4 class="subtitle">March 26, 2019</h4>
                <h3 class="p_title"><p>The black boy</p></h3>


                <p class="description">把身边的朋友用表情包的方式记录下来.</p>
                <a class="links" target="view_window" href="https://sticker.weixin.qq.com/cgi-bin/mmemoticon-bin/emoticonview?oper=single&t=shop/detail&productid=aL2PCfwK/89qO7sF6/+I+UDhfwEjhec2ZNvdnLLJRd/P+mGNBQzw/wHGohl1VY5TGQnRn4PEVtiHiYOy8mKZcFevo1y9cRGVEWDVjw2AWZSc=">Visit website &gt;</a>
            </div>
        </div>
    </div>


    <div class="columns">
        <div class="list">
            <img class="p_img_4 is-5" src="http://img.6chen.cn/image/1.svg" data-action="zoom" style="cursor: -webkit-zoom-in;">
            <img class="p_img_4 is-5" src="http://img.6chen.cn/image/2.svg" data-action="zoom" style="cursor: -webkit-zoom-in;">
            <img class="p_img_4 is-5" src="http://img.6chen.cn/image/3.svg" data-action="zoom" style="cursor: -webkit-zoom-in;">
            <img class="p_img_4 is-5" src="http://img.6chen.cn/image/4.svg" data-action="zoom" style="cursor: -webkit-zoom-in;">
        </div>
    </div>

    <div class="title">
        <h4 class="subtitle">February 12, 2019</h4>
        <h3 class="p_title"><p>每周练习</p></h3>
        <p class="description">Weekly practice for Adobe XD</p>


    </div>



    <div class="columns">
        <div class="list">
            <img class="p_img" src="http://img.6chen.cn/image/work_05.png" data-action="zoom" style="cursor: -webkit-zoom-in;">
        </div>
    </div>

    <div class="title">
        <h4 class="subtitle">july 13, 2018</h4>
        <h3 class="p_title"><p>YOUXIAKE</p></h3>
        <p class="img_info">游侠客微信小程序旅游商城</p>



    </div>



    <div class="columns">
        <div class="list">
            <img class="p_img" src="http://img.6chen.cn/image/work_08.jpg" data-action="zoom" style="cursor: -webkit-zoom-in;">
        </div>
    </div>

    <div class="title">
        <h4 class="subtitle">March 26, 2018</h4>
        <h3 class="p_title"><p>User date</p></h3>


        <p class="description">2018游侠客用户数据h5.</p>
        <a class="links" target="view_window" href="https://h5.youxiake.com/2018/user/#/">Visit website &gt;</a>
    </div>
</div>
</div>







    <div class="columns">
        <div class="list">
            <img class="p_img" src="http://img.6chen.cn/image/work_09.jpg" data-action="zoom" style="cursor: -webkit-zoom-in;">
        </div>
    </div>

    <div class="title">
        <h4 class="subtitle">May 11, 2017</h4>
        <h3 class="p_title"><p>游侠客旅游网</p></h3>


        <p class="description">对整站进行了改版设计，从登录注册，首页到线路详情页等进行了产品模块优化.重新制定了设计规范，建立了ui控件库.</p>
        <a class="links" target="view_window" href="http://www.youxiake.com/">Visit website &gt;</a>
    </div>









    <div class="columns">
        <div class="list">
            <img class="p_img_4 is-5" src="http://img.6chen.cn/image/work_03.png" data-action="zoom" style="cursor: -webkit-zoom-in;">
            <img class="p_img_4 is-5" src="http://img.6chen.cn/image/work_04.png" data-action="zoom" style="cursor: -webkit-zoom-in;">
        </div>
    </div>

    <div class="title">
        <h4 class="subtitle">December 16, 2017</h4>
        <h3 class="p_title"><p>Logo Design</p></h3>
        <p class="description">予舍珠宝.（原阿里1000号员工艾米创立的珠宝品牌）</p>


    </div>




    <div class="columns">
        <div class="list">
            <img class="p_img" src="http://img.6chen.cn/image/work_100.jpg" data-action="zoom" style="cursor: -webkit-zoom-in;">
        </div>
    </div>

    <div class="title">
        <h4 class="subtitle">March 12, 2012</h4>
        <h3 class="p_title"><p>良渚君澜酒店官网</p></h3>
        <p class="img_info">Web Desgin</p>

        <a class="links" target="view_window" href="http://www.naradalz.com/">Visit website &gt;</a>

    </div>








                </div>
            </nav>
        </div>
    </div>

    <div class="footer">
        <div class="container has-text-centered">
          <p>Designed and Coded by © CaiCai
          <br><a href="http://www.beian.miit.gov.cn/" target=_blank>浙ICP备19043988号</a></p>
        </div>
    </div>
@endsection
