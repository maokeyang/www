<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>莲生妙相</title>

    <!-- Scripts -->
    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}

    <!-- Fonts -->
    {{--<link rel="dns-prefetch" href="//fonts.gstatic.com">--}}
    {{--<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">--}}

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="I am a designer">
    <meta name="keywords" content="liansheng,UI,UX,Dribbble,UI Designer,App,Interaction, Icon, Web, Ant Design,UED,Css">
    <meta content="liansheng · Designer" property="og:title">
    <title>liansheng · Designer</title>
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@liansheng">
    <meta name="twitter:title" content="liansheng,Designer">
    <meta name="author" content="liansheng,hi@liansheng">
    <meta name="twitter:description" content="My name is liansheng based in China. Now, work for youxiake.">
    <meta name="twitter:image" content="http://img.6chen.cn/%E6%9C%AA%E6%A0%87%E9%A2%98-1.png">
    <meta name="apple-mobile-web-app-capable" content="liansheng">


    <!-- Styles -->
    <link rel="shortcut icon" href="http://img.6chen.cn/1.png">
    <link rel="apple-touch-icon" href="http://img.6chen.cn/1.png">
    <link rel="stylesheet" href="https://unpkg.com/griddd@1.0.2/dist/griddd.min.css">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link href="//cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

</head>
<body>
    @yield('content')

    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script src="{{ asset('js/zooming.js') }}" defer></script>

</body>
</html>
